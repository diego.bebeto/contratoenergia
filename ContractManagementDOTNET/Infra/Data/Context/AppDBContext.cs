﻿using Microsoft.EntityFrameworkCore;
using ContractManagementDOTNET.Domain.Entities;
using ContractManagementDOTNET.Infra.Data.Mapping;

namespace ContractManagementDOTNET.Infra.Data.Context
{
    public class AppDBContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<EnergyContract> EnergyContracts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer("Server=(local);Database=ContractManagement;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(new UserMap(modelBuilder).Configure);

            modelBuilder.Entity<EnergyContract>(new EnergyContractMap(modelBuilder).Configure);
        }
    }
}
