﻿using ContractManagementDOTNET.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;


namespace ContractManagementDOTNET.Infra.Data.Mapping
{
    //Created by Diego Benedetti - 28/04/2020
    public class EnergyContractMap : IEntityTypeConfiguration<EnergyContract>
    {
        #region Constructor Entity User
        public EnergyContractMap(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EnergyContract>()
            .HasKey(ec => ec.idContract);

            modelBuilder.Entity<EnergyContract>()
            .HasIndex(ec => new { ec.idContract, ec.nameClient })
            .IsUnique()
            .HasName("Index_EnergyContract");
        }
        #endregion


        #region Mapping the fields of table Bot in method Configure. Utilized the library EntiryFrameWorkCore
        public void Configure(EntityTypeBuilder<EnergyContract> builder)
        {
            builder.ToTable("EnergyContract");

            builder.Property(ec => ec.idContract)
            .HasColumnType("uniqueidentifier")
            .IsRequired();

            builder.Property(ec => ec.nameClient)
                .HasColumnName("nameClient")
                .HasMaxLength(220)
                .HasColumnType("varchar(220)")
                .IsRequired();

            builder.Property(ec => ec.contractType)
                .HasColumnName("contractType")
                .HasMaxLength(1)
                .HasColumnType("varchar(1)")
                .IsRequired();


            builder.Property(ec => ec.qtyNegotiated)
                .HasColumnName("qtyNegotiated")
                .HasColumnType("int")
                .IsRequired();

            builder.Property(ec => ec.durationContract)
                .HasColumnName("durationContract")
                .HasColumnType("int")
                .IsRequired();

            builder.Property(ec => ec.archiveContract)
                .HasColumnName("archiveContract")
                .HasColumnType("varchar(MAX)")
                .IsRequired();
        }
        #endregion
    }
}