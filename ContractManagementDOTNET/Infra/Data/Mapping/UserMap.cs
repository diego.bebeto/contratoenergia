﻿using ContractManagementDOTNET.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;


namespace ContractManagementDOTNET.Infra.Data.Mapping
{
    //Created by Diego Benedetti - 28/04/2020
    public class UserMap : IEntityTypeConfiguration<User>
    {
        #region Constructor Entity User
        public UserMap(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .HasKey(u => u.cpf);

            modelBuilder.Entity<User>()
            .HasKey(u => u.email);

            modelBuilder.Entity<User>()
            .HasIndex(u => new { u.cpf,u.email })
            .IsUnique()
            .HasName("Index_User");
        }
        #endregion


        #region Mapping the fields of table Bot in method Configure. Utilized the library EntiryFrameWorkCore
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder.Property(u => u.cpf)
            .HasColumnType("varchar(11)")
            .IsRequired();

            builder.Property(u => u.fullName)
                .HasColumnName("fullName")
                .HasMaxLength(50)
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(u => u.password)
                .HasColumnName("password")
                .HasMaxLength(50)
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(u => u.email)
                .HasColumnName("email")
                .IsRequired();

            builder.Property(u => u.createdAt)
                .HasColumnName("createdAt")
                .HasColumnType("datetime")
                .IsRequired();
        }
        #endregion
    }
}