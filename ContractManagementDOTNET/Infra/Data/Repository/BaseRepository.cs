﻿using ContractManagementDOTNET.Infra.Data.Context;
using ContractManagementDOTNET.Domain.Interface;
using ContractManagementDOTNET.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace ContractManagementDOTNET.Infra.Data.Repository 
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private AppDBContext context = new AppDBContext();

        public async Task<int> Insert(T entity)
        {
            context.Set<T>().Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> Remove(T entity)
        {
            context.Set<T>().Remove(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<T> Select(int Id)
        {
            return await context.Set<T>().FindAsync(Id);
        }

        public async Task<IEnumerable<T>> SelectAll()
        {
            return await context.Set<T>().ToAsyncEnumerable().ToList();
        }

        public async Task<int> Update(T entity)
        {
            context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            return await context.SaveChangesAsync();
        }
    }
}
