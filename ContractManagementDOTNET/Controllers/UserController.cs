﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ContractManagementDOTNET.Domain.Entities;
using ContractManagementDOTNET.Infra.Data.Context;
using ContractManagementDOTNET.Service.InterfaceServices;
using ContractManagementDOTNET.Service.Validators;

namespace ContractManagementDOTNET.Controllers
{
    public class UserController : Controller
    {
        private readonly BaseService<User> service;

        public UserController()
        {
            service = new BaseService<User>();
        }

        
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IEnumerable<User>> GetAll()
        {
            return await service.GetAll();
        }

        public async Task<IActionResult> Post([FromForm] User item)
        {
            try
            {
                item.createdAt = DateTime.Now;

                await service.Post<UserValidator>(item);

                return View();
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public async Task<IActionResult> Put([FromForm] User item)
        {
            try
            {
                await service.Put<UserValidator>(item);

                return new ObjectResult(item);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public async Task<IActionResult> Delete([FromForm] User item)
        {
            try
            {
                await service.Delete(item);

                return new NoContentResult();
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }   

        public async Task<IActionResult> ValidUser([FromForm] User user)
        {
            try
            {
                var userFind = await service.GetAll();
                var userFound = userFind.ToList().Find(u => u.cpf == user.cpf && u.password == user.password);

                if (userFound == null)
                    userFound = userFind.ToList().Find(u => u.email == user.cpf && u.password == user.password);

                if (userFound != null)
                    return RedirectToAction("Index","Contract");
                else
                {
                    View("~/Views/User/Index.cshtml").TempData["Message"] = "Falha ao autenticar o usuario. Verifique os dados e tente novamente!";
                    return RedirectToAction("Index","User");
                }                    
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public async Task<IActionResult> Get(int id)
        {
            try
            {
                return new ObjectResult(await service.Get(id));
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}