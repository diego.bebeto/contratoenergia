﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ContractManagementDOTNET.Domain.Entities;
using ContractManagementDOTNET.Infra.Data.Context;
using ContractManagementDOTNET.Service.Validators;
using ContractManagementDOTNET.Service.InterfaceServices;

namespace ContractManagementDOTNET.Controllers
{
    public class ContractController : Controller
    {
        private readonly BaseService<EnergyContract> service;

        public ContractController()
        {
            service = new BaseService<EnergyContract>();
        }
    

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }


        public async Task<IEnumerable<EnergyContract>> GetAll()
        {
            return await service.GetAll();
        }

        public async Task<IActionResult> Post([FromForm] EnergyContract item)
        {
            try
            {
                await service.Post<EnergyContractValidator>(item);

                return RedirectToAction("Index");
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public async Task<IActionResult> Put([FromForm] EnergyContract item)
        {
            try
            {
                await service.Put<EnergyContractValidator>(item);

                return new ObjectResult(item);
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public async Task<IActionResult> Delete(EnergyContract item)
        {
            try
            {
                await service.Delete(item);

                return new NoContentResult();
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public async Task<IActionResult> Get(int id)
        {
            try
            {
                return new ObjectResult(await service.Get(id));
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}