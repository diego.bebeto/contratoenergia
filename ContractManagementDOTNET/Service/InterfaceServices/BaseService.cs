﻿using ContractManagementDOTNET.Domain.Entities;
using ContractManagementDOTNET.Domain.Interface;
using ContractManagementDOTNET.Infra.Data.Repository;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContractManagementDOTNET.Service.InterfaceServices
{
    public class BaseService<T> : IService<T> where T : BaseEntity
    {
        private BaseRepository<T> baseRepository = new BaseRepository<T>();

        public async Task<int> Delete(T obj)
        {
            return await baseRepository.Remove(obj);
        }

        public async Task<T> Get(int id)
        {
            return await baseRepository.Select(id);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await baseRepository.SelectAll();
        }

        public async Task<int> Post<V>(T obj) where V : AbstractValidator<T>
        {
            return await baseRepository.Insert(obj);
        }

        public async Task<int> Put<V>(T obj) where V : AbstractValidator<T>
        {
            return await baseRepository.Update(obj);
        }
    }
}
