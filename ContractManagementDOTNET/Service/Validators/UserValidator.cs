﻿using ContractManagementDOTNET.Domain.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContractManagementDOTNET.Service.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        #region In constructor is created the validation rules for entity in your respective fields web
        public UserValidator()
        {
            RuleFor(u => u)
                .NotNull()
                .OnAnyFailure(ex =>
                {
                    throw new ArgumentNullException("A classe usuário não pode ser vazia.");
                });

            RuleFor(u => u.cpf)
                .NotNull().WithMessage("O cpf não pode ser vazio.");

            RuleFor(u => u.email)
                .NotNull().WithMessage("O email não pode ser vazio.");

            RuleFor(u => u.fullName)
                .NotNull().WithMessage("Favor informar o nome do completo.")
                .NotEmpty().WithMessage("Favor informar o nome do completo.");

            RuleFor(u => u.password)
                .NotNull().WithMessage("Favor informar a senha do usuário.")
                .NotEmpty().WithMessage("Favor informar a do usuário.");

            RuleFor(u => u.createdAt)
                .NotNull().WithMessage("Favor informar a data de cadastro.")
                .NotEmpty().WithMessage("Favor informar a data de cadastro.");
        }
        #endregion
    }
}
