﻿using ContractManagementDOTNET.Domain.Entities;
using FluentValidation;
using System;

namespace ContractManagementDOTNET.Service.Validators
{
    public class EnergyContractValidator : AbstractValidator<EnergyContract>
    {
        #region In constructor is created the validation rules for entity in your respective fields web
        public EnergyContractValidator()
        {
            RuleFor(ec => ec)
                .NotNull()
                .OnAnyFailure(ex =>
                {
                    throw new ArgumentNullException("A classe não pode ser vazia.");
                });

            RuleFor(ec => ec.idContract)
                .NotNull().WithMessage("O identificador do contrato não pode ser vazio.");

            RuleFor(ec => ec.nameClient)
                .NotNull().WithMessage("Favor informar o nome do cliente.")
                .NotEmpty().WithMessage("Favor informar o nome do cliente.");

            RuleFor(ec => ec.contractType)
                .NotNull().WithMessage("Favor informar o tipo do contrato.")
                .NotEmpty().WithMessage("Favor informar o tipo do contrato.");

            RuleFor(ec => ec.qtyNegotiated)
                .NotNull().WithMessage("Favor informar a quantidade de negociação.")
                .NotEmpty().WithMessage("Favor informar a quantidade de negociação.");

            RuleFor(ec => ec.durationContract)
                .NotNull().WithMessage("Favor informar a duração do contrato.")
                .NotEmpty().WithMessage("Favor informar a duração do contrato.");

            RuleFor(ec => ec.archiveContract)
                .NotNull().WithMessage("Favor informar o arquivo do contrato.")
                .NotEmpty().WithMessage("Favor informar o arquivo do contrato.");
        }
        #endregion
    }
}
