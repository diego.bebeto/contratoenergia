﻿using ContractManagementDOTNET.Domain.Entities;
using FluentValidation;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContractManagementDOTNET.Domain.Interface
{
    //Created by Diego Benedetti - 30/03/2020
    public interface IService<T> where T : BaseEntity
    {
        #region Interface what manipulate the CRUD, in which receive one object <T> (generic) in methods for handle all entities. All methods contains return async in thread.
        //Post request from a database entity
        Task<int> Post<V>(T obj) where V : AbstractValidator<T>;

        //Put request from a database entity
        Task<int> Put<V>(T obj) where V : AbstractValidator<T>;

        //Delete request from a database entity
        Task<int> Delete(T obj);

        //Get request from a database entity
        Task<T> Get(int id);

        //Get request  all data from a database entity
        Task<IEnumerable<T>> GetAll();
        #endregion
    }
}
