﻿using ContractManagementDOTNET.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContractManagementDOTNET.Domain.Interface
{
    //Created by Diego Benedetti - 30/03/2020
    public interface IRepository<T> where T : BaseEntity
    {
        #region Interface CRUD in which receive one object<T>(generic) in methods for handle all entities. All methods contains return async in thread.
        //Method for insert one record.
        Task<int> Insert(T entity);

        //Method for update one record.
        Task<int> Update(T entity);

        //Method for remove one record.
        Task<int> Remove(T entity);

        //Method for select one record.
        Task<T> Select(int Id);

        //Method for select all records.
        Task<IEnumerable<T>> SelectAll();
        #endregion
    }
}
