﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Text;

namespace ContractManagementDOTNET.Domain.Entities
{
    [Table("EnergyContract", Schema = "dbo")]
    [DebuggerStepThrough]
    //Created by Diego Benedetti - 28/04/2020
    public class EnergyContract : BaseEntity
    {
        #region Create Entities of the table EnergyContract  
        //Manipulation (get and set) of the field idContract of the table EnergyContract in Data Base
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "obrigatório informar a propriedade: Id do Contrato de energia", AllowEmptyStrings = false)]
        [Column("idContract", Order = 1, TypeName = "uniqueidentifier")]
        public Guid idContract { get; set; }

        //Manipulation (get and set) of the field nameClient of the table EnergyContract in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: Nome do contrato", AllowEmptyStrings = false)]
        [Column("nameClient", Order = 2, TypeName = "varchar(220)")]
        public string nameClient { get; set; }

        //Manipulation (get and set) of the field contractType of the table EnergyContract in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: tipo do contrato", AllowEmptyStrings = false)]
        [Column("contractType", Order = 3, TypeName = "varchar(1)")]
        public string contractType { get; set; }

        //Manipulation (get and set) of the field qtyNegotiated of the table EnergyContract in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: quantidade de negociação", AllowEmptyStrings = false)]
        [Column("qtyNegotiated", Order = 4, TypeName = "int")]
        public int qtyNegotiated { get; set; }

        //Manipulation (get and set) of the field durationContract of the table EnergyContract in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: duração de contrato", AllowEmptyStrings = false)]
        [Column("durationContract", Order = 5, TypeName = "int")]
        public int durationContract { get; set; }

        //Manipulation (get and set) of the field contractType of the table EnergyContract in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: arquivo", AllowEmptyStrings = false)]
        [Column("archiveContract", Order = 6, TypeName = "varchar(1)")]
        public string archiveContract { get; set; }
        #endregion
    }
}