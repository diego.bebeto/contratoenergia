﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Text;

namespace ContractManagementDOTNET.Domain.Entities
{
    [Table("User", Schema = "dbo")]
    [DebuggerStepThrough]
    //Created by Diego Benedetti 28/04/2020
    public class User : BaseEntity
    {
        #region Create Entities of the table User  
        //Manipulation (get and set) of the field cpf of the table User in Data Base
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "obrigatório informar a propriedade: CPF", AllowEmptyStrings = false)]
        [Column("cpf", Order = 1, TypeName = "varchar(11)")]
        public string cpf { get; set; }

        //Manipulation (get and set) of the field fullName of the table User in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: Nome completo do usuário", AllowEmptyStrings = false)]
        [Column("fullName", Order = 2, TypeName = "varchar(50)")]
        public string fullName { get; set; }

        //Manipulation (get and set) of the field password of the table User in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: Senha do usuário", AllowEmptyStrings = false)]
        [Column("password", Order = 3, TypeName = "varchar(50)")]
        public string password { get; set; }

        //Manipulation (get and set) of the field email of the table User in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: email", AllowEmptyStrings = false)]
        [Column("email", Order = 4, TypeName = "varchar(80)")]
        public string email { get; set; }


        //Manipulation (get and set) of the field createdAt of the table User in Data Base
        [Required(ErrorMessage = "obrigatório informar a propriedade: Data criação", AllowEmptyStrings = false)]
        [Column("createdAt", Order = 5, TypeName = "datetime")]
        public DateTime createdAt { get; set; }
        #endregion
    }
}