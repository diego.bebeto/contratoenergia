CREATE DATABASE ContractManagement

GO

USE ContractManagement

GO

CREATE TABLE [User](
	cpf			VARCHAR(11) NOT NULL,
	fullName	VARCHAR(50) NOT NULL,
	password	VARCHAR(50) NOT NULL,
	email		VARCHAR(80) NOT NULL,
	status		BIT NOT NULL,
	createdAt	DATETIME NOT NULL,
 CONSTRAINT PK_User PRIMARY KEY (cpf,email ASC)); 

GO

CREATE INDEX Index_User
ON [User](cpf,email)

GO

CREATE TABLE EnergyContract(
	idContract		 UNIQUEIDENTIFIER NOT NULL,
	nameClient		 VARCHAR(220)	  NOT NULL,
	contractType	 VARCHAR(1)		  NOT NULL,
	qtyNegotiated	 INT		      NOT NULL,
	durationContract INT			  NOT NULL,
	archiveContract  VARCHAR(MAX)	  NOT NULL,
CONSTRAINT PK_EnergyContract PRIMARY KEY (idContract ASC));

GO 

CREATE INDEX Index_EnergyContract
ON EnergyContract( idContract, nameClient);